# -*- coding: utf-8 -*-
import pygame
from pygame.rect import Rect


class GameUi():
    def __init__(self, screen, hero):
        self.screen = screen
        self.screen.drawable.append(self)
        self.hero = hero
        self.width = 100
        self.height = 10
        self.position = ()
        self.z_index = 50000

    def draw(self, window):
        self.draw_bar(window, self.hero.hp, 0, (0, 255, 0))
        self.draw_bar(window, self.hero.energy, 1, (200, 200, 0))


    def draw_bar(self, window, value, index, color):
        pygame.draw.rect(window, color, Rect(20, 20 + index * self.height, self.width * value / 100., self.height))
        pygame.draw.rect(window, (0, 0, 0), Rect(20, 20 + index * self.height, self.width, self.height), 1)