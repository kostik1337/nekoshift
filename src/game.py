#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame
import sys
from menuscreen import MenuScreen

import config


class Game():
    def __init__(self):
        pygame.init()
        self.window = pygame.display.set_mode((config.screen_width, config.screen_height))
        pygame.display.set_caption('NekoShift')
        self.screen = None
        self.next_screen = None
        self.change_screen(MenuScreen())

        clock = pygame.time.Clock()
        while 1:
            clock.tick(config.fps)
            if self.next_screen is not None:
                self.screen = self.next_screen
                self.next_screen = None

            for event in pygame.event.get():
                self.screen.event(event)
                self.event(event)
            self.screen.update(pygame.time.get_ticks())

            self.window.fill((0, 0, 0))
            self.screen.draw()
            pygame.display.flip()

    def event(self, event):
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYUP and event.key == pygame.K_ESCAPE:
            sys.exit()

    def change_screen(self, screen):
        self.next_screen = screen
        self.next_screen.set_parent(self)
