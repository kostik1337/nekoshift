# -*- coding: utf-8 -*-
import pygame
import config

loaded_images = {}
loaded_sounds = {}
pygame.font.init()
fonts = {
    # "04b_21__.ttf": pygame.font.Font("res/04b_21__.ttf", 15),
    "menu": pygame.font.Font("res/04b_25__.ttf", 25),
    "menu_small": pygame.font.Font("res/04b_25__.ttf", 16),
    "game": pygame.font.Font("res/04b_30__.ttf", 25)
}
# font = pygame.font.Font("res/arcadeclassic.ttf", 15)


def load_image(image):
    if image in loaded_images:
        return loaded_images[image]
    else:
        surf = pygame.image.load(image).convert()
        surf.set_colorkey(pygame.Color(config.transparent_colorkey[0], config.transparent_colorkey[1],
                                       config.transparent_colorkey[2], 0))
        if config.pixel_multiplier > 1:
            surf = pygame.transform.scale(surf, (surf.get_width() * config.pixel_multiplier,
                                                 surf.get_height() * config.pixel_multiplier))
        loaded_images[image] = surf
        return surf


def load_text(text, font):
    return fonts[font].render(text, 1, (255, 255, 255))

last_sound = ""
last_time = 0

def play_sound(sound_file):
    # A real dirty-hacker way to avoid multiple sounds at one time
    global last_sound, last_time
    time = pygame.time.get_ticks()
    if last_sound == sound_file and time-last_time <100:
        return
    last_sound = sound_file
    last_time = time

    if sound_file in loaded_sounds:
        sound = loaded_sounds[sound_file]
    else:
        sound = pygame.mixer.Sound(sound_file)
        loaded_sounds[sound_file] = sound
    sound.play()