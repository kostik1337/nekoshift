# -*- coding: utf-8 -*-
import random
import pygame
from sprites import AbstractSprite


class LightningBolt(AbstractSprite):
    ORIENTATION_LEFT = 0
    ORIENTATION_UP = 1
    ORIENTATION_RIGHT = 2
    ORIENTATION_DOWN = 3
    width = 20

    def __init__(self, screen, x, y, l, orientation):
        AbstractSprite.__init__(self, screen, x, y)
        screen.updateable.append(self)

        self.orientation = orientation
        if orientation == LightningBolt.ORIENTATION_LEFT:
            self.end_x = x - l
            self.end_y = y
            self.bbox = pygame.Rect(- l, - LightningBolt.width / 2, l, LightningBolt.width)
        elif orientation == LightningBolt.ORIENTATION_UP:
            self.end_x = x
            self.end_y = y - l
            self.bbox = pygame.Rect( - LightningBolt.width / 2, 0 - l, LightningBolt.width, l)
        elif orientation == LightningBolt.ORIENTATION_RIGHT:
            self.end_x = x + l
            self.end_y = y
            self.bbox = pygame.Rect(0, - LightningBolt.width / 2, l, LightningBolt.width)
        elif orientation == LightningBolt.ORIENTATION_DOWN:
            self.end_x = x
            self.end_y = y + l
            self.bbox = pygame.Rect(- LightningBolt.width / 2, 0, LightningBolt.width, l)

        self.num_points = 5
        self.points = []
        for i in range(self.num_points):
            self.points.append([i * (self.end_x - x) / self.num_points, i * (self.end_y - y) / self.num_points])

    def update(self):
        rand_max = 10
        for point in self.points[1:]:
            point[0] = min(max(point[0] + random.randint(-rand_max, rand_max), self.bbox.left), self.bbox.right)
            point[1] = min(max(point[1] + random.randint(-rand_max, rand_max), self.bbox.top), self.bbox.bottom)

    def draw(self, window):
        lst = [[l[0] + self.x, l[1] + self.y] for l in self.points]
        pygame.draw.lines(window, (0, 255, 255), False, lst, 3)
        pygame.draw.lines(window, (255, 255, 255), False, lst)
        # pygame.draw.line(window, (0, 255, 255), False, self.points)
        # pygame.draw.rect(window, (0,255,0), self.bbox.move(self.x, self.y), 1)

