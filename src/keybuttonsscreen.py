# -*- coding: utf-8 -*-

from screen import Screen
from sprites import *


class KeyButtonsScreen(Screen):
    def __init__(self):
        Screen.__init__(self)

        self.selected_index = 0
        self.buttons = []

    def add_button(self, button, func):
        self.buttons.append((button, func))

    def event(self, event):
        Screen.event(self, event)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                index = (self.selected_index - 1) % len(self.buttons)
            elif event.key == pygame.K_DOWN:
                index = (self.selected_index + 1) % len(self.buttons)
            elif event.key == pygame.K_RETURN:
                self.buttons[self.selected_index][1]()
                return
            else:
                return
            self.buttons[self.selected_index][0].set_selected(False)
            self.buttons[index][0].set_selected(True)
            self.selected_index = index