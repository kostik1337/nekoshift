# -*- coding: utf-8 -*-
import pygame


class Screen():
    def __init__(self):
        self.drawable = []
        self.clickable = []
        self.updateable = []

    def draw(self):
        for sprite in sorted(self.drawable, key=lambda spr: spr.z_index):
            sprite.draw(self.parent.window)

    def event(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            for c in self.clickable:
                if c.is_clicked(event.pos[0], event.pos[1]):
                    c.call_on_click()

    def update(self, time):
        for upd in self.updateable:
            upd.update()

    def set_parent(self, parent):
        self.parent = parent