# -*- coding: utf-8 -*-
import config
import gamescreen
from keybuttonsscreen import KeyButtonsScreen
import levels
import menuscreen
import resourcemanager

from screen import *
from sprites import *
import wrapline


class TextScreen(KeyButtonsScreen):
    TO_MENU = -2
    TO_FIRST_PRELEVEL = -1

    def __init__(self, next_level, text=None):
        KeyButtonsScreen.__init__(self)

        if text == None:
            l = levels.get_level(next_level)
            text = l.prelevel_text
            next_level = l.index

        self.init_text(text)
        Sprite(self, "res/menu-text.png")
        self.next_level = next_level
        if next_level >= 0:
            self.add_button(KeyboardButton(self, 320, config.screen_height - 70, "play", "menu"), self.play)
        elif next_level == TextScreen.TO_FIRST_PRELEVEL:
            self.add_button(KeyboardButton(self, 320, config.screen_height - 70, "play", "menu"), self.first_prelevel)
        elif next_level == TextScreen.TO_MENU:
            self.add_button(KeyboardButton(self, 320, config.screen_height - 70, "back to menu", "menu"), self.menu)
        self.buttons[self.selected_index][0].set_selected(True)

    def init_text(self, about_text):
        padding = 40
        lines = wrapline.wrap_multi_line(about_text, resourcemanager.fonts["menu_small"],
                                         config.screen_width - padding * 2)
        self.surf = pygame.Surface((config.screen_width, config.screen_height), pygame.SRCALPHA)
        self.surf.fill((0, 0, 0, 0))
        # self.surf.set_colorkey((0, 0, 0))
        y_offset = padding
        for l in lines:
            line_surface = resourcemanager.load_text(l, "menu_small")
            self.surf.blit(line_surface, (padding, y_offset))
            y_offset += line_surface.get_height() + 4

    def play(self):
        self.parent.change_screen(gamescreen.GameScreen(self.next_level))

    def menu(self):
        self.parent.change_screen(menuscreen.MenuScreen())

    def first_prelevel(self):
        self.parent.change_screen(TextScreen(0))

    def draw(self):
        Screen.draw(self)
        self.parent.window.blit(self.surf, (0, 0))
