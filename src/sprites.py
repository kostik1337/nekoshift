# -*- coding: utf-8 -*-

import pygame
import resourcemanager
import config

screen_rect = pygame.Rect(0, 0, config.screen_width, config.screen_height)


class AbstractSprite():
    def __init__(self, screen, x, y):
        screen.drawable.append(self)
        self.x = x
        self.y = y
        self.width = 0
        self.height = 0
        self.z_index = -1

    def set_position(self, x, y):
        self.x = x
        self.y = y

    def set_size(self, width, height):
        self.width = width
        self.height = height

    def update(self):
        pass

    def draw(self, window):
        pass

    def set_z_index(self, z_index):
        self.z_index = z_index

    def remove(self, screen):
        screen.drawable.remove(self)
        if self in screen.updateable:
            screen.updateable.remove(self)

    # Attempt of premature optimization
    def is_on_screen(self):
        return True  # screen_rect.colliderect(pygame.Rect(self.x, self.y, self.x + self.width, self.y + self.height))


class Sprite(AbstractSprite):
    def __init__(self, screen, image_file, x=0, y=0):
        AbstractSprite.__init__(self, screen, x, y)
        self.base_image = self.image = resourcemanager.load_image(image_file)
        self.width = self.image.get_width()
        self.height = self.image.get_height()

    def set_size(self, width, height):
        AbstractSprite.set_size(self, width, height)
        self.image = pygame.transform.scale(self.image, (width, height))

    def draw(self, window):
        if self.is_on_screen(): window.blit(self.image, (self.x, self.y))


class SpriteSheet(AbstractSprite):
    def __init__(self, screen, image_file, num_frames_x, num_frames_y, frame, x=0, y=0):
        AbstractSprite.__init__(self, screen, x, y)
        self.base_image = self.image = resourcemanager.load_image(image_file)
        self.width = self.image.get_width() / num_frames_x
        self.height = self.image.get_height() / num_frames_y
        self.num_frames = (num_frames_x, num_frames_y)
        self.frame = frame

    def draw(self, window):
        if self.is_on_screen():
            area_rect = pygame.Rect(self.width * self.frame[0], self.height * self.frame[1], self.width, self.height)
            window.blit(self.image, (self.x, self.y), area=area_rect)


class AnimatedSprite(SpriteSheet):
    def __init__(self, screen, image_file, num_frames, delay=config.animation_delay, x=0, y=0):
        SpriteSheet.__init__(self, screen, image_file, num_frames, 1, [0, 0], x, y)
        self.delay = delay
        screen.updateable.append(self)
        self.world_frame = 0
        self.animations = {}
        self.current_animation = None
        self.current_animation_name = None
        self.enabled = True

    def set_enabled(self, enabled):
        self.enabled = enabled

    def add_animation(self, name, interval):
        self.animations[name] = interval

    def set_animation(self, name):
        if name != self.current_animation_name:
            self.current_animation_name = name
            self.current_animation = name in self.animations and self.animations[name] or (0,0)
            self.frame = [self.current_animation[0], 0]

    def update(self):
        if self.enabled:
            AbstractSprite.update(self)
            self.world_frame += 1
            if self.world_frame % self.delay == 0:
                self.frame[0] += 1
                if self.frame[0] > self.current_animation[1]:
                    self.frame[0] = self.current_animation[0]

    def draw(self, window):
        if self.enabled:
            SpriteSheet.draw(self, window)


class Button(AbstractSprite):
    def __init__(self, screen, x, y, width, height, text):
        AbstractSprite.__init__(self, screen, x, y)
        screen.clickable.append(self)
        self.text_surface = resourcemanager.load_text(text)
        self.width = width
        self.height = height
        self.onClick = None

    def draw(self, window):
        pygame.draw.rect(window, (255, 0, 0), pygame.Rect(self.x, self.y, self.width, self.height))
        window.blit(self.text_surface, (self.x + self.width / 2 - self.text_surface.get_width() / 2,
                                        self.y + self.height / 2 - self.text_surface.get_height() / 2))

    def set_on_click(self, onClick):
        self.onClick = onClick

    def call_on_click(self):
        if self.onClick is not None:
            self.onClick()

    def is_clicked(self, x, y):
        return pygame.Rect(self.x, self.y, self.width, self.height).collidepoint(x, y)


class KeyboardButton(AbstractSprite):
    def __init__(self, screen, x, y, text, font):
        AbstractSprite.__init__(self, screen, x, y)
        self.text_surface = resourcemanager.load_text(text, font)
        self.rtriangle_surface = resourcemanager.load_text(">", font)
        self.ltriangle_surface = resourcemanager.load_text("<", font)
        self.width = self.text_surface.get_width()
        self.height = self.text_surface.get_height()
        self.is_selected = False

    def draw(self, window):
        window.blit(self.text_surface, (self.x - self.text_surface.get_width() / 2, self.y))
        if self.is_selected:
            window.blit(self.rtriangle_surface,
                        (self.x - self.text_surface.get_width() / 2 - self.rtriangle_surface.get_width() - 5, self.y))
            window.blit(self.ltriangle_surface,
                        (self.x + self.text_surface.get_width() / 2 + self.ltriangle_surface.get_width() - 5, self.y))

    def set_selected(self, is_selected):
        self.is_selected = is_selected