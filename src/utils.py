# -*- coding: utf-8 -*-
import pygame
import config


def surface_color(surface, c):
    for i in range(surface.get_width()):
        for j in range(surface.get_height()):
            color = surface.get_at((i, j))
            if not (color.r, color.g, color.b) == config.transparent_colorkey:
                color.r, color.g, color.b = c
                surface.set_at((i, j), color)
    return surface

def distance_squared(p1, p2):
    dx = p2[0] - p1[0]
    dy = p2[1] - p1[1]
    return dx * dx + dy * dy


def rect_collides_line(rect, p1, p2):
    if p1[1] == p2[1]:
        if rect.top < p1[1] and rect.bottom > p1[1] and rect.centerx > min(p1[0], p2[0]) and rect.centerx < max(p1[0],
                                                                                                                p2[0]):
            return True
        else:
            return False
    elif p1[0] == p2[0]:
        if rect.left < p1[0] and rect.right > p1[0] and rect.centery > min(p1[1], p2[1]) and rect.centery < max(p1[1],
                                                                                                                p2[1]):
            return True
        else:
            return False
    raise NotImplemented