# -*- coding: utf-8 -*-

screen_width = 640
screen_height = 480
fps = 60
transparent_colorkey = (0, 0, 0)
pixel_multiplier = 2
animation_delay = 7
tile_size = 32 * pixel_multiplier
debug_enabled = True
debug_output = False
player_invincible_frames = 89
invincible_blink_rate = 15

about_text = """Once upon a time there was a little kitty called Chouhiro-nyan.
She lived in the Green Forest, eating mushrooms and apples, keeping the forest out of troubles.
One day, evil spirits have appeared in the woods to make the forest their haunt.
As the guardian of the Green Forest, Chouhiro-nyan bears a duty to banish these filthy spirits out of her home!
Chouhiro-nyan seeks for amber stones to transform herself in a magical girl for a short time and obtain superpowers.
She has ability to cast lightning charges while in catgirl form to expel your foes.
Beware! Spirits bite. Keep an eye on your HP level and use food to refill your health.
Save the Green Forest from doom!

Controls: arrow keys - move
Space - transform in a magical girl and fire lightnings"""

die_text = "Oops, you died. Try again?"