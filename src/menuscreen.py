# -*- coding: utf-8 -*-
import sys
import config
from gamescreen import GameScreen
from keybuttonsscreen import KeyButtonsScreen

from sprites import *
from screen import *
from textscreen import TextScreen


class MenuScreen(KeyButtonsScreen):
    def __init__(self):
        KeyButtonsScreen.__init__(self)

        pygame.mixer.music.stop()
        pygame.mixer.music.load("res/sounds/menu.ogg")
        pygame.mixer.music.play(-1)

        Sprite(self, "res/menu-bg.png")
        self.add_button(KeyboardButton(self, 320, 220, "play", "menu"), self.play)
        self.add_button(KeyboardButton(self, 320, 260, "about", "menu"), self.about)
        self.buttons[self.selected_index][0].set_selected(True)

    def play(self):
        self.parent.change_screen(TextScreen(0))

    def about(self):
        self.parent.change_screen(TextScreen(TextScreen.TO_FIRST_PRELEVEL, config.about_text))