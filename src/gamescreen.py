# -*- coding: utf-8 -*-
from math import sqrt

import pygame
import random
from pygame.rect import Rect
from config import *
from game_ui import GameUi
import levels
from levels import LevelObjective
from lightningbolt import LightningBolt
import menuscreen
from sprites import *
from screen import *
import textscreen
import utils


class GameObjectSpriteData():
    data_dict = {}
    static_objects = []

    def __init__(self, name, collide_rect_width, collide_rect_height, collide_rect_x_offset, collide_rect_y_offset,
                 width_tiles=1, height_tiles=1):
        self.name = name
        self.collide_rect_width = collide_rect_width
        self.collide_rect_height = collide_rect_height
        self.collide_rect_x_offset = collide_rect_x_offset
        self.collide_rect_y_offset = collide_rect_y_offset
        self.width_tiles = width_tiles
        self.height_tiles = height_tiles
        GameObjectSpriteData.data_dict[name] = self


GameObjectSpriteData("res/bush-med.png", 0.42, 0.3, 0.0, 0.1)
GameObjectSpriteData("res/chop-med.png", 0.6, 0.35, 0.0, 0.1)
GameObjectSpriteData("res/chop-small.png", 0.75, 0.6, 0.0, 0.25)
GameObjectSpriteData("res/stone-med.png", 0.8, 0.7, 0.0, 0.2)
GameObjectSpriteData("res/stone-small.png", 0.6, 0.5, 0.1, 0.15)
GameObjectSpriteData("res/tree-1-large.png", 0.2, 0.2, 0.05, 0.05)
GameObjectSpriteData("res/tree-2-large.png", 0.3, 0.17, 0.12, 0.08)

GameObjectSpriteData("res/pebble.png", 0.5, 1.0, 0.0, 0.0, height_tiles=2)
GameObjectSpriteData("res/pebble-2.png", 0.8, 0.5, 0.0, 0.0, width_tiles=2)
GameObjectSpriteData("res/fallen.png", 0.8, 0.5, 0.0, 0.0, width_tiles=4)
GameObjectSpriteData("res/port.png", 1.0, 1.0, 0.0, 0.0, width_tiles=2, height_tiles=2)

GameObjectSpriteData("res/kitty.png", 0.33, 0.33, 0.0, 0.33)
GameObjectSpriteData("res/nekospirit.png", 0.33, 0.33, 0.0, 0.33)
GameObjectSpriteData("res/spirit.png", 0.33, 0.33, 0.0, 0.33)
GameObjectSpriteData("res/person.png", 0.33, 0.45, 0.0, 0.2)
GameObjectSpriteData("res/stuffs.png", 1.0, 1.0, 0.0, 0.0)

GameObjectSpriteData.static_objects = [
    GameObjectSpriteData.data_dict["res/bush-med.png"],
    GameObjectSpriteData.data_dict["res/chop-med.png"],
    GameObjectSpriteData.data_dict["res/chop-small.png"],
    GameObjectSpriteData.data_dict["res/stone-med.png"],
    GameObjectSpriteData.data_dict["res/stone-small.png"],
    GameObjectSpriteData.data_dict["res/tree-1-large.png"],
    GameObjectSpriteData.data_dict["res/pebble.png"],
    GameObjectSpriteData.data_dict["res/pebble-2.png"],
    GameObjectSpriteData.data_dict["res/fallen.png"]
]


class GameObject():
    def __init__(self, screen, gosd=None):
        screen.updateable.append(self)
        screen.drawable.append(self)
        self.z_index = 10000
        self.screen = screen
        self.position = [0, 0]
        self.sprite_offset = (0, 0)
        self.sprite = None
        self.collide_rect = None

        if gosd is not None:
            self.init_gosd(gosd)

    def set_position_tile(self, position_tile):
        self.position = [tile_size * position_tile[0] + tile_size * self.width_tiles / 2,
                         tile_size * position_tile[1] - tile_size * self.height_tiles / 2 + tile_size]
        self.collide_rect.center = (self.position[0], self.position[1])

    def init_gosd(self, gosd):
        self.collide_rect_width = gosd.collide_rect_width
        self.collide_rect_x_offset = gosd.collide_rect_x_offset
        self.collide_rect_height = gosd.collide_rect_height
        self.collide_rect_y_offset = gosd.collide_rect_y_offset
        self.width_tiles = gosd.width_tiles
        self.height_tiles = gosd.height_tiles

    def update(self):
        self.sprite.set_position(self.position[0] + self.sprite_offset[0] - self.screen.camera_position[0],
                                 self.position[1] + self.sprite_offset[1] - self.screen.camera_position[1])
        self.sprite.z_index = self.position[1]

    def init_collide_rect(self, sprite):
        self.collide_rect = pygame.Rect(0, 0, sprite.width * self.collide_rect_width,
                                        sprite.height * self.collide_rect_height)
        self.collide_rect.inflate_ip(-4 * pixel_multiplier, -4 * pixel_multiplier)
        self.sprite_offset = (-sprite.width / 2 + sprite.width * self.collide_rect_x_offset,
                              -sprite.height + self.collide_rect.height / 2 + sprite.height * self.collide_rect_y_offset)

    def collides_with(self, object):
        return self.collide_rect.colliderect(object.collide_rect)

    def draw(self, window):
        if debug_output:
            pygame.draw.rect(window, (0, 255, 0), Rect(self.collide_rect).move(
                (-self.screen.camera_position[0], -self.screen.camera_position[1])), 1)
            pygame.draw.circle(window, (255, 0, 0), (int(self.position[0]) - self.screen.camera_position[0],
                                                     int(self.position[1]) - self.screen.camera_position[1]), 1)

    def remove(self):
        self.screen.updateable.remove(self)
        self.screen.drawable.remove(self)
        self.screen.game_objects.remove(self)
        self.sprite.remove(self.screen)


class Background(GameObject):
    def __init__(self, screen, sprite_file, position, frame):
        GameObject.__init__(self, screen)
        self.sprite = SpriteSheet(screen, sprite_file, 3, 3, frame)
        self.position = list(position)
        screen.drawable.remove(self)

    def update(self):
        GameObject.update(self)
        self.sprite.z_index = 0


class StaticObject(GameObject):
    def __init__(self, screen, gosd):
        GameObject.__init__(self, screen, gosd)
        self.sprite = Sprite(screen, gosd.name)
        self.init_collide_rect(self.sprite)


class PortalObject(StaticObject):
    def __init__(self, screen):
        gosd = GameObjectSpriteData.data_dict["res/port.png"]
        StaticObject.__init__(self, screen, gosd)
        self.sprite.remove(screen)
        self.sprite = AnimatedSprite(screen, gosd.name, 8)
        self.sprite.add_animation("main", (0, 7))
        self.sprite.set_animation("main")
        self.init_collide_rect(self.sprite)
        self.frames = 0
        self.spawn_period = 240
        self.hp = 100

    def update(self):
        GameObject.update(self)
        self.sprite.z_index = self.position[1] - 50
        self.frames += 1
        if self.frames % self.spawn_period == 0:
            tile_position = [self.position[0] / tile_size, self.position[1] / tile_size]
            self.screen.spawn_enemy((tile_position[0] + random.randint(-1, 1), tile_position[1] + random.randint(-1, 1)))

    def damage(self, damage_hp):
        self.hp -= damage_hp
        if self.hp <= 0:
            self.remove()
            self.screen.increase_objective(LevelObjective.PORTAL_CLOSE)


class BonusObject(StaticObject):
    def __init__(self, screen, index):
        gosd = GameObjectSpriteData.data_dict["res/stuffs.png"]
        StaticObject.__init__(self, screen, gosd)
        self.sprite.remove(screen)
        self.sprite = SpriteSheet(screen, gosd.name, 4, 1, index)
        self.init_collide_rect(self.sprite)


class HealthBonus(BonusObject):
    def __init__(self, screen, food_index):
        BonusObject.__init__(self, screen, (food_index, 0))
        self.amount = 20


class EnergyBonus(BonusObject):
    def __init__(self, screen):
        BonusObject.__init__(self, screen, (3, 0))
        self.amount = 25


class MovingGameObject(GameObject):
    def __init__(self, screen, position, gosd):
        GameObject.__init__(self, screen, gosd)
        self.position = list(position)
        self.speed = [0, 0]
        self.max_speed = (0, 0)

    def check_collisions(self):
        is_collided_x = False
        is_collided_y = False
        for obj in self.screen.game_objects:
            if obj is not self and not isinstance(obj, BonusObject) and not isinstance(obj, PortalObject) \
                    and obj.collide_rect is not None:
                expand_factor = 2 * pixel_multiplier
                x_added_rect = Rect(self.collide_rect).move(self.speed[0] * expand_factor, 0)
                y_added_rect = Rect(self.collide_rect).move(0, self.speed[1] * expand_factor)
                if obj.collide_rect.colliderect(x_added_rect) or x_added_rect.right > \
                                self.screen.field_width - tile_size or x_added_rect.left < tile_size:
                    is_collided_x = True
                if obj.collide_rect.colliderect(y_added_rect) or y_added_rect.bottom > \
                                self.screen.field_height - tile_size or y_added_rect.top < tile_size:
                    is_collided_y = True
        return (is_collided_x, is_collided_y)

    def update(self):
        is_collided_x, is_collided_y = self.check_collisions()

        if not is_collided_x:
            self.position[0] += self.speed[0] * self.max_speed[0]
        if not is_collided_y:
            self.position[1] += self.speed[1] * self.max_speed[1]

        self.collide_rect.center = (self.position[0], self.position[1])
        GameObject.update(self)


class Hero(MovingGameObject):
    CAT_STATE = 0
    NEKO_STATE = 1
    CASTING_STATE = 2
    AFTER_CASTING_STATE = 3
    AFTER_CASTING_COOLDOWN_STATE = 4

    def __init__(self, screen, position):
        self.init_sprites(screen)
        MovingGameObject.__init__(self, screen, position, self.cat_gosd)

        self.sprite = self.cat_sprite
        self.sprite.set_enabled(True)
        self.state = Hero.CAT_STATE
        self.init_collide_rect(self.sprite)
        self.neko_max_speed = (1 * pixel_multiplier, 1 * pixel_multiplier)
        self.cat_max_speed = (2 * pixel_multiplier, 2 * pixel_multiplier)
        self.max_speed = self.cat_max_speed
        self.hp = 100
        self.energy = 0
        self.invincible_frames = 0
        self.direction = "down"
        self.bolt = None
        self.max_cast_frames = 30
        self.after_cast_frames = 30
        self.after_cast_cooldown_frames = 120
        self.energy_loss_frames = 15
        self.frame = 0

    def init_sprites(self, screen):
        self.neko_gosd = GameObjectSpriteData.data_dict["res/person.png"]
        self.cat_gosd = GameObjectSpriteData.data_dict["res/kitty.png"]
        self.dead_cat_gosd = GameObjectSpriteData.data_dict["res/nekospirit.png"]

        self.neko_sprite = AnimatedSprite(screen, self.neko_gosd.name, 64)
        self.neko_sprite.add_animation("down", (0, 3))
        self.neko_sprite.add_animation("left", (4, 7))
        self.neko_sprite.add_animation("up", (8, 11))
        self.neko_sprite.add_animation("right", (12, 15))
        self.neko_sprite.add_animation("idle_down", (16, 23))
        self.neko_sprite.add_animation("idle_left", (24, 31))
        self.neko_sprite.add_animation("idle_up", (32, 39))
        self.neko_sprite.add_animation("idle_right", (40, 47))
        self.neko_sprite.add_animation("cast_down", (48, 51))
        self.neko_sprite.add_animation("cast_left", (52, 55))
        self.neko_sprite.add_animation("cast_up", (56, 59))
        self.neko_sprite.add_animation("cast_right", (60, 63))
        self.neko_sprite.set_enabled(False)

        self.cat_sprite = AnimatedSprite(screen, self.cat_gosd.name, 48)
        self.cat_sprite.add_animation("down", (0, 3))
        self.cat_sprite.add_animation("left", (4, 7))
        self.cat_sprite.add_animation("up", (8, 11))
        self.cat_sprite.add_animation("right", (12, 15))
        self.cat_sprite.add_animation("idle_down", (16, 23))
        self.cat_sprite.add_animation("idle_left", (24, 31))
        self.cat_sprite.add_animation("idle_up", (32, 39))
        self.cat_sprite.add_animation("idle_right", (40, 47))
        self.cat_sprite.set_enabled(False)

        self.dead_cat_sprite = AnimatedSprite(screen, self.dead_cat_gosd.name, 16)
        self.dead_cat_sprite.add_animation("down", (0, 3))
        self.dead_cat_sprite.add_animation("left", (4, 7))
        self.dead_cat_sprite.add_animation("up", (8, 11))
        self.dead_cat_sprite.add_animation("right", (12, 15))
        self.dead_cat_sprite.add_animation("idle_down", (0, 0))
        self.dead_cat_sprite.add_animation("idle_left", (4, 4))
        self.dead_cat_sprite.add_animation("idle_up", (8, 8))
        self.dead_cat_sprite.add_animation("idle_right", (12, 12))
        self.dead_cat_sprite.set_enabled(False)

    def change_form(self, is_neko):
        is_now_neko = self.state != Hero.CAT_STATE
        if is_now_neko != is_neko:
            resourcemanager.play_sound(is_neko and "res/sounds/evolve2.wav" or "res/sounds/evolve1.wav")
            self.state = is_neko and Hero.NEKO_STATE or Hero.CAT_STATE
            self.sprite.set_enabled(False)
            self.sprite = is_neko and self.neko_sprite or self.cat_sprite
            self.sprite.set_enabled(True)
            self.init_gosd(is_neko and self.neko_gosd or self.cat_gosd)
            self.init_collide_rect(self.sprite)
            self.max_speed = is_neko and self.neko_max_speed or self.cat_max_speed

    def transform_to_neko(self):
        if self.energy == 100:
            self.change_form(True)

    def set_x_speed(self, speed):
        self.speed[0] = speed

    def set_y_speed(self, speed):
        self.speed[1] = speed

    def invincible(self):
        return self.invincible_frames > 0

    def is_dead(self):
        return self.sprite == self.dead_cat_sprite

    def damage(self, d):
        resourcemanager.play_sound("res/sounds/damage.wav")
        self.invincible_frames = player_invincible_frames
        self.blink_image = utils.surface_color(self.sprite.image.copy(), (255, 255, 255))
        self.sprite.image = self.blink_image
        self.hp -= d

    def increase_hp(self, amount):
        self.hp = min(self.hp + amount, 100)

    def increase_energy(self, amount):
        self.energy = min(self.energy + amount, 100)

    def find_bolt_objects(self, bolt_direction):
        objects = []
        for b in self.screen.game_objects:
            p1 = self.position
            d = screen_width
            if bolt_direction == LightningBolt.ORIENTATION_LEFT:
                p2 = (p1[0] - d, p1[1])
            elif bolt_direction == LightningBolt.ORIENTATION_RIGHT:
                p2 = (p1[0] + d, p1[1])
            elif bolt_direction == LightningBolt.ORIENTATION_UP:
                p2 = (p1[0], p1[1] - d)
            elif bolt_direction == LightningBolt.ORIENTATION_DOWN:
                p2 = (p1[0], p1[1] + d)

            if utils.rect_collides_line(b.collide_rect.inflate(50, 50), p1, p2):
                objects.append(b)

        static_objects = filter(lambda obj: isinstance(obj, StaticObject), objects)
        enemies = filter(lambda obj: isinstance(obj, Enemy), objects)
        closest_static_object = None
        if len(static_objects) > 0:
            closest_static_object = \
                min(static_objects, key=lambda obj: utils.distance_squared(obj.position, self.position))

            if len(enemies) > 0:
                enemies = filter(lambda obj: utils.distance_squared(obj.position, self.position) <
                                             utils.distance_squared(closest_static_object.position, self.position),
                                 enemies)

        return (closest_static_object, enemies)

    def cast_lightning(self):
        if self.state == Hero.NEKO_STATE:
            self.state = Hero.CASTING_STATE
            self.cast_frames = self.max_cast_frames

    def shoot(self):
        resourcemanager.play_sound("res/sounds/pew.wav")
        if self.bolt != None:
            self.bolt.remove(self.screen)
            self.bolt = None

        bolt_direction = self.direction == "left" and LightningBolt.ORIENTATION_LEFT or \
                         self.direction == "right" and LightningBolt.ORIENTATION_RIGHT or \
                         self.direction == "up" and LightningBolt.ORIENTATION_UP or \
                         self.direction == "down" and LightningBolt.ORIENTATION_DOWN

        bolt_objects = self.find_bolt_objects(bolt_direction)
        bolt_l = bolt_objects[0] == None and screen_width or sqrt(
            utils.distance_squared(bolt_objects[0].collide_rect.center, self.position))
        if isinstance(bolt_objects[0], PortalObject):
            bolt_objects[0].damage(50)

        for enemy in bolt_objects[1]:
            enemy.kill()
            self.screen.increase_objective(LevelObjective.ENEMY_KILL)
        self.bolt = LightningBolt(self.screen, self.position[0] - self.screen.camera_position[0],
                                  self.position[1] - self.screen.camera_position[1], bolt_l, bolt_direction)
        self.bolt.set_z_index(
            bolt_direction == LightningBolt.ORIENTATION_UP and self.position[1] - 2 or self.position[1])

    def draw(self, window):
        GameObject.draw(self, window)
        if self.state == Hero.CASTING_STATE:
            progress = 1.0 - float(self.cast_frames) / self.max_cast_frames
            radius = int(max(40 * (1.0 - progress), 4))
            pygame.draw.circle(window, (0, 255, 255), (self.position[0] - self.screen.camera_position[0],
                                                       self.position[1] - self.screen.camera_position[1] - 16),
                               radius, min(int(4.0 * progress + 1), radius))

    def check_animation(self):
        if self.speed[0] > 0:
            self.direction = "right"
        elif self.speed[0] < 0:
            self.direction = "left"
        elif self.speed[1] > 0:
            self.direction = "down"
        elif self.speed[1] < 0:
            self.direction = "up"

        is_moving = self.speed[0] != 0 or self.speed[1] != 0
        if is_moving:
            self.sprite.set_animation(self.direction)
        else:
            if self.state == Hero.CASTING_STATE or self.state == Hero.AFTER_CASTING_STATE:
                prefix = "cast_"
            else:
                prefix = "idle_"
            self.sprite.set_animation(prefix + self.direction)

    def check_states(self):
        if self.state == Hero.CASTING_STATE:
            self.cast_frames -= 1
            if self.cast_frames == 0:
                self.shoot()
                self.state = Hero.AFTER_CASTING_STATE
                self.cast_frames = self.after_cast_frames
        elif self.state == Hero.AFTER_CASTING_STATE:
            self.cast_frames -= 1
            if self.cast_frames == 0:
                self.bolt.remove(self.screen)
                self.bolt = None
                self.state = Hero.AFTER_CASTING_COOLDOWN_STATE
                self.cast_frames = self.after_cast_cooldown_frames
        elif self.state == Hero.AFTER_CASTING_COOLDOWN_STATE:
            self.cast_frames -= 1
            if self.cast_frames == 0:
                self.state = Hero.NEKO_STATE
        elif self.state == Hero.NEKO_STATE:
            if self.frame % self.energy_loss_frames == 0:
                self.energy -= 1
                if self.energy == 0:
                    self.change_form(False)

        if self.invincible():
            self.invincible_frames -= 1
            if self.invincible_frames % invincible_blink_rate == 0:
                self.sprite.image = self.sprite.image == self.blink_image and self.sprite.base_image or self.blink_image

            if self.invincible_frames == 0:
                self.sprite.image = self.sprite.base_image

    def update(self):
        self.frame += 1
        if self.state == Hero.CASTING_STATE or self.state == Hero.AFTER_CASTING_STATE:
            self.speed = [0, 0]
        MovingGameObject.update(self)

        self.check_states()
        self.check_animation()
        for bonus in filter(lambda obj: isinstance(obj, BonusObject), self.screen.game_objects):
            if bonus.collide_rect.colliderect(self.collide_rect):
                resourcemanager.play_sound("res/sounds/pick.wav")
                if isinstance(bonus, HealthBonus):
                    self.increase_hp(bonus.amount)
                elif isinstance(bonus, EnergyBonus):
                    self.increase_energy(bonus.amount)
                bonus.remove()


class Enemy(MovingGameObject):
    IDLE_STATE = 0
    CHASE_STATE = 1
    ATTACK_STATE = 2
    DYING_STATE = 3

    def __init__(self, screen, position):
        gosd = GameObjectSpriteData.data_dict["res/spirit.png"]
        MovingGameObject.__init__(self, screen, position, gosd)
        self.sprite = AnimatedSprite(screen, gosd.name, 16)
        self.sprite.add_animation("idle", (12, 15))
        self.sprite.add_animation("down", (0, 3))
        self.sprite.add_animation("left", (4, 7))
        self.sprite.add_animation("up", (8, 11))
        self.sprite.add_animation("right", (12, 15))
        self.init_collide_rect(self.sprite)
        self.max_chase_speed = (0.85 * pixel_multiplier, 0.85 * pixel_multiplier)
        self.max_idle_speed = (0.5 * pixel_multiplier, 0.5 * pixel_multiplier)
        self.max_speed = self.max_idle_speed
        self.hero = self.screen.hero
        self.state = Enemy.IDLE_STATE
        self.max_death_frames = 40

        self.time_running_new_direction = 0
        self.prev_positions = []

        self.attack_dist = 50
        self.chase_dist = 300

    def attack(self):
        if not self.hero.invincible():
            self.hero.damage(20)

    def update(self):
        MovingGameObject.update(self)
        if self.state == Enemy.DYING_STATE:
            self.speed = [0, 0]
            self.death_frames -= 1
            if self.death_frames <= 0:
                self.remove()
            else:
                self.sprite.height = self.base_sprite_height * self.death_frames / self.max_death_frames

        else:
            vec_to_hero = (self.hero.position[0] - self.position[0], self.hero.position[1] - self.position[1])
            distance_sq = vec_to_hero[0] * vec_to_hero[0] + vec_to_hero[1] * vec_to_hero[1]
            if distance_sq <= self.attack_dist * self.attack_dist and not self.hero.is_dead():
                self.state = Enemy.ATTACK_STATE
            elif distance_sq <= self.chase_dist * self.chase_dist and not self.hero.is_dead():
                self.state = Enemy.CHASE_STATE
            else:
                self.state = Enemy.IDLE_STATE

            if self.state == Enemy.ATTACK_STATE:
                self.speed = [0, 0]
                self.attack()
            elif self.state == Enemy.CHASE_STATE:
                self.max_speed = self.max_chase_speed
                self.prev_positions.append(tuple(self.position))
                if len(self.prev_positions) > 4: del self.prev_positions[0]

                if self.time_running_new_direction > 0:
                    self.time_running_new_direction -= 1
                else:
                    if abs(vec_to_hero[0]) < 5:
                        self.speed[0] = 0
                    else:
                        self.speed[0] = vec_to_hero[0] > 0 and 1 or -1

                    if abs(vec_to_hero[1]) < 5:
                        self.speed[1] = 0
                    else:
                        self.speed[1] = vec_to_hero[1] > 0 and 1 or -1

                if len(self.prev_positions) > 3 and self.prev_positions[-1] == self.prev_positions[-2] == \
                        self.prev_positions[-3]:
                    r = random.randint(0, 1) == 0 and -1 or 1
                    self.speed[0], self.speed[1] = -self.speed[1] * r, self.speed[0] * r
                    self.time_running_new_direction = 60

            elif self.state == Enemy.IDLE_STATE:
                self.max_speed = self.max_idle_speed
                if random.randint(0, 240) == 0:
                    self.speed = [random.randint(-1, 1), random.randint(-1, 1)]

            if self.speed[0] > 0:
                self.sprite.set_animation("right")
            elif self.speed[0] < 0:
                self.sprite.set_animation("left")
            elif self.speed[1] > 0:
                self.sprite.set_animation("down")
            elif self.speed[1] < 0:
                self.sprite.set_animation("up")
            else:
                self.sprite.set_animation("idle")


    def kill(self):
        resourcemanager.play_sound("res/sounds/kill.wav")
        self.state = Enemy.DYING_STATE
        self.death_frames = self.max_death_frames
        self.base_sprite_height = self.sprite.height


class GameScreen(Screen):
    def __init__(self, level_number=0):
        Screen.__init__(self)
        self.backgrounds = []
        self.hero = Hero(self, (0, 0))
        self.game_objects = [self.hero]
        self.camera_position = [0, 0]
        self.game_ui = GameUi(self, self.hero)

        pygame.mixer.music.stop()
        pygame.mixer.music.load("res/sounds/the_green_forest.ogg")
        pygame.mixer.music.play(-1)

        self.level = levels.get_level(level_number)
        self.level_objectives = map(lambda obj: obj.clear(), self.level.objectives)
        self.init_level(self.level)
        self.after_end_func = None
        self.end_frames = 0
        self.max_end_frames = 120

    def increase_objective(self, type):
        for obj in self.level_objectives:
            if obj.objective_type == type:
                obj.increase_count()

        if len(filter(lambda obj: not obj.is_done(), self.level_objectives)) == 0:
            self.end_frames = self.max_end_frames
            self.after_end_func = self.next_level_menu

    def init_grass(self):
        for i in range(self.field_width_tiles):
            for j in range(self.field_height_tiles):
                if i == 0:
                    frame_x = 0
                elif i == self.field_width_tiles - 1:
                    frame_x = 2
                else:
                    frame_x = 1
                if j == 0:
                    frame_y = 0
                elif j == self.field_height_tiles - 1:
                    frame_y = 2
                else:
                    frame_y = 1
                self.backgrounds.append(
                    Background(self, "res/grass9.png", (i * tile_size, j * tile_size), (frame_x, frame_y)))

    def init_level(self, level):
        self.field_width_tiles, self.field_height_tiles = level.size
        self.field_width, self.field_height = self.field_width_tiles * tile_size, self.field_height_tiles * tile_size
        self.init_grass()

        self.hero.position = [level.start_pos[0] * tile_size + tile_size / 2,
                              level.start_pos[1] * tile_size + tile_size / 2]
        for item in level.map_objects:
            res_name = item[0]
            if res_name == "amber":
                object = EnergyBonus(self)
            elif "food" in res_name:
                object = HealthBonus(self, int(res_name[-1]) - 1)
            elif res_name == "spirit.png":
                object = Enemy(self, (0, 0))
            elif res_name == "port.png":
                object = PortalObject(self)
            else:
                obj_data = GameObjectSpriteData.data_dict["res/" + res_name]
                object = StaticObject(self, obj_data)
            object.set_position_tile((item[1], item[2]))
            self.game_objects.append(object)

    def init_random_level(self):
        self.field_width_tiles, self.field_height_tiles = (16 * random.randint(1, 3), 16 * random.randint(1, 3))
        self.field_width, self.field_height = self.field_width_tiles * tile_size, self.field_height_tiles * tile_size
        self.init_grass()

        portal = PortalObject(self)
        portal.set_position_tile((16, 16))
        self.game_objects.append(portal)

        for i in range(5):
            self.spawn_enemy(
                (random.randint(1, self.field_width_tiles - 1), random.randint(1, self.field_height_tiles - 1)))

        for i in range(30):
            bonus = random.randint(0, 1) == 0 and HealthBonus(self) or EnergyBonus(self)
            bonus.set_position_tile(
                (random.randint(1, self.field_width_tiles - 1), random.randint(1, self.field_height_tiles - 1)))
            self.game_objects.append(bonus)

        for i in range(20):
            obj_data = GameObjectSpriteData.static_objects[
                random.randint(0, len(GameObjectSpriteData.static_objects) - 1)]
            static_object = StaticObject(self, obj_data)

            is_valid = False
            while not is_valid:
                static_object.set_position_tile(
                    (random.randint(1, self.field_width_tiles - 1), random.randint(1, self.field_height_tiles - 1)))
                is_valid = True
                for obj in self.game_objects:
                    if static_object.collides_with(obj):
                        is_valid = False
            self.game_objects.append(static_object)

    def spawn_enemy(self, position_tile):
        enemy = Enemy(self, (0, 0))
        self.game_objects.append(enemy)
        enemy.set_position_tile(position_tile)


    def update_camera_position(self):
        if self.hero.position[0] < screen_width / 2:
            self.camera_position[0] = 0
        elif self.hero.position[0] > self.field_width - screen_width / 2:
            self.camera_position[0] = self.field_width - screen_width
        else:
            self.camera_position[0] = self.hero.position[0] - screen_width / 2

        if self.hero.position[1] < screen_height / 2:
            self.camera_position[1] = 0
        elif self.hero.position[1] > self.field_height - screen_height / 2:
            self.camera_position[1] = self.field_height - screen_height
        else:
            self.camera_position[1] = self.hero.position[1] - screen_height / 2

    def update(self, time):
        Screen.update(self, time)
        self.update_camera_position()
        self.handle_movement()
        if self.end_frames > 0:
            self.end_frames -= 1
            if self.end_frames == 0:
                self.after_end_func()
        elif self.hero.hp <= 0:
            resourcemanager.play_sound("res/sounds/oh_no.wav")

            self.hero.sprite.set_enabled(False)
            self.hero.sprite = self.hero.dead_cat_sprite
            self.hero.sprite.set_enabled(True)
            self.hero.init_gosd(self.hero.dead_cat_gosd)
            self.hero.init_collide_rect(self.hero.sprite)
            self.hero.invincible_frames = 0

            self.end_frames = self.max_end_frames
            self.after_end_func = self.die_menu

    def die_menu(self):
        self.parent.change_screen(textscreen.TextScreen(self.level.index, config.die_text))
        pygame.mixer.music.stop()
        pygame.mixer.music.load("res/sounds/menu.ogg")
        pygame.mixer.music.play(-1)

    def next_level_menu(self):
        self.parent.change_screen(textscreen.TextScreen(self.level.index + 1))
        pygame.mixer.music.stop()
        pygame.mixer.music.load("res/sounds/menu.ogg")
        pygame.mixer.music.play(-1)

    def handle_movement(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_LEFT] and not pressed[pygame.K_RIGHT]:
            self.hero.set_x_speed(-1)
        elif pressed[pygame.K_RIGHT] and not pressed[pygame.K_LEFT]:
            self.hero.set_x_speed(1)
        else:
            self.hero.set_x_speed(0)

        if pressed[pygame.K_UP] and not pressed[pygame.K_DOWN]:
            self.hero.set_y_speed(-1)
        elif pressed[pygame.K_DOWN] and not pressed[pygame.K_UP]:
            self.hero.set_y_speed(1)
        else:
            self.hero.set_y_speed(0)

    def event(self, event):
        Screen.event(self, event)
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_SPACE:
                if self.hero.state == Hero.CAT_STATE:
                    self.hero.transform_to_neko()
                else:
                    self.hero.cast_lightning()
            elif event.key == pygame.K_d:
                if debug_enabled:
                    global debug_output
                    debug_output = not debug_output


    def draw(self):
        Screen.draw(self)
        if debug_output:
            for i in range(self.field_width_tiles):
                pygame.draw.line(self.parent.window, (0, 255, 0),
                                 (i * tile_size - self.camera_position[0], -self.camera_position[1]),
                                 (i * tile_size - self.camera_position[0], self.field_height - self.camera_position[1]))
            for i in range(self.field_height_tiles):
                pygame.draw.line(self.parent.window, (0, 255, 0),
                                 (- self.camera_position[0], i * tile_size - self.camera_position[1]),
                                 (self.field_width - self.camera_position[0], i * tile_size - self.camera_position[1]))
            self.parent.window.blit(resourcemanager.load_text("%d %d" % (self.hero.position[0] / tile_size,
                                                                         self.hero.position[1] / tile_size),
                                                              "menu"), (100, 100))
